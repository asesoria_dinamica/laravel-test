@extends('templates.base')

@section('body')
@if( empty($user) )
<form class="ajax" action="{{ url('/usuarios') }}" method="post" autocomplete="off">
@else
<form class="" action="{{ url('/usuarios/'.$user->id) }}" method="post" autocomplete="off">
  @method('put')
@endif
  @csrf
  <div class="form-group">
    <label for="">nombre</label>
    <input type="text" name="username" value="{{ !empty($user) ? $user->username : '' }}" autocomplete="off">
    <ul class="errors"></ul>
  </div>

  <div class="form-group">
    <label for="">correo</label>
    <input type="text" name="email" value="{{ !empty($user) ? $user->email : '' }}" autocomplete="off">
    <ul class="errors"></ul>
  </div>

  <div class="form-group">
    <label for="">contra</label>
    <input type="password" name="password" autocomplete="off">
    <ul class="errors"></ul>
  </div>

  <button type="submit" name="button">enviar</button>
</form>
@endsection
