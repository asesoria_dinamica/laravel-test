@extends('templates.base')

@section('body')
<h1>Usuarios</h1>
<h2>${{ number_format($mi_var,2) }}</h2>

<a href="{{ url('/usuarios/nuevo') }}">nuevo</a>
<table>
  <thead>
    <tr>
      <th>Correo</th>
      <th>Nombre</th>
      <th></th>
    </tr>
  </thead>

  <tbody>
    @foreach($users as $u)
    <tr>
      <td>{{ $u->email }}</td>
      <td>{{ $u->username }}</td>
      <td>
        <a href="{{ url('/usuarios/nuevo/'.$u->id) }}">editar</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection
