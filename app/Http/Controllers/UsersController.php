<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index() {
      $mi_var = 10000.0567;

      $users = \App\User::all();
      return view('usuarios', compact("mi_var", "users"));
    }

    public function userForm($id = null) {
      $user = null;

      if ( !empty($id) ) {
        $user = \App\User::find($id);
      }

      return view('user_form', compact('user'));
    }

    public function postUser(Request $req) {
      $this->validate($req, [
        "username" => "required",
        "email" => "required|email",
        "password" => "required"
      ]);

      $username = $req->input('username');
      $email = $req->input('email');
      $password = $req->input('password');

      $user = new \App\User();
      $user->username = $username;
      $user->email = $email;
      $user->password = Hash::make($password);//bcrypt()
      $user->save();

      return redirect('/usuarios');
    }

    public function putUser(Request $req, $id) {
      $user = \App\User::findOrFail($id);

      $username = $req->input('username');
      $email = $req->input('email');
      $password = $req->input('password');

      $user->username = $username;
      $user->email = $email;
      $user->password = Hash::make($password);

      $user->save();

      return redirect('/usuarios');
    }

    public function sandbox() {
      $pass = request()->input("pass");
      $pass_hash = Hash::make($pass);
      dd($pass_hash);
    }
}
