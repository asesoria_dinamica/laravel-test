<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/usuarios', 'UsersController@index');
Route::post('/usuarios', 'UsersController@postUser');
Route::put('/usuarios/{id}', 'UsersController@putUser');
Route::get('/usuarios/nuevo', 'UsersController@userForm');
Route::get('/usuarios/nuevo/{id}', 'UsersController@userForm');
//Route::get('/usuarios/{id}', 'UsersController@userForm');

Route::any('/sandbox', 'UsersController@sandbox');
