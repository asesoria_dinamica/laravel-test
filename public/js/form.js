$(document).ready(function () {
  $("form.ajax").on("submit", function (e) {
    e.preventDefault();

    var form = $(this);
    var data = new FormData(form[0]);

    form.find('.has-error').removeClass('.has-error');
    form.find('.invalid').removeClass('.invalid');

    form.find('.errors').html('');

    $.ajax({
      url: form.attr('action'),
      method: form.attr('method'),
      dataType: 'json',
      data: data,
      cache: false,
      contentType: false,
      processData: false
    }).done(function (result) {
    }).fail(function (jqXHR, textStatus, errorThrown) {
      $('.loadingMsg').fadeOut();
      switch (jqXHR.status) {
        case 422:
          var errors = jqXHR.responseJSON.errors;
          for ( var key in errors ) {
            var field = form.find('[name="' + key + '"]');
            var group = field.closest('.form-group');
            var ul = group.find('.errors');

            field.addClass('invalid');
            group.addClass('has-error');

            for (var i = 0; i < errors[key].length; i++) {
              ul.append('<li>' + errors[key][i] + '</li>');
            }

          }

          console.log(errors);
          break;
        default:
          break;
      }
      form.find('button').removeClass('disabled');
    });

    return false;
  });
});
